// Das ist ein einzeiliger Kommentar
/*Das ist ein mehrzeiliger Kommentar*/

//Das Express-Framework wird eingebunden
//Ein Framework soll die programmierung erleichtern.
//Das Framework muss mit NPM installiert werden:
//Im Terminal npm Install express --save
const bodyparser = require('body-parser')

const express = require('express')

//Das Appobjekt wird initialisiert
//Das Appobjekt representiert den Server
//Auf das Appobjekt werden im Folgenden Methoden Aufgerufen

const app = express()

//Mit der ejs.view engine werden Werte von der Server.js zu Index-datei gegeben

app. set('view engine', 'ejs')

app.use(bodyparser.urlencoded({extended: true}))

app.use(express.static('public'))

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Der Server ist erfolgreich gestartet PORT %s', server.address().port)    })
app.get('/',(req,res, next) => {
    res.render('index.ejs', {

    })
})